package dev.easylist.auth

import org.ektorp.http.StdHttpClient
import org.ektorp.impl.StdCouchDbInstance
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.netflix.zuul.EnableZuulProxy
import org.springframework.context.annotation.Bean
import javax.net.ssl.HttpsURLConnection


@SpringBootApplication
@EnableZuulProxy
@EnableDiscoveryClient
class AuthApplication {

    @Value("\${couchDB.url}")
    val url: String? = null
    @Value("\${couchDB.username}")
    val uname: String? = null
    @Value("\${couchDB.password}")
    val pwd: String? = null
    @Value("\${couchDB.dbname}")
    val dbname: String? = null

    @Bean
    fun createDatabaseIfNotExist () {
        val httpClient = StdHttpClient.Builder()
                .url(url)
                .username(uname)
                .password(pwd)
                .build()

        val dbInstance = StdCouchDbInstance(httpClient)
        dbInstance.createConnector(dbname, true)
    }
}

fun main(args: Array<String>) {
    runApplication<AuthApplication>(*args)

    HttpsURLConnection.setDefaultHostnameVerifier { hostname, session -> true }

}
