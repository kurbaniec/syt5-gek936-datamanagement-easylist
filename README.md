# EasyList - The simple shopping list

### *Datamanagement - Synchronisation*

## Task description
The detailed [task description](TASK.md) contains all information for realization.

## Implementation

Our approach to the task and design decisions can be found [here]().

## :question:What is EasyList

EasyList is a easy shopping list application that allows access from multiple applications. A React-App provides a user-friendly interface to edit your shopping-entries. A Spring and Kotlin back-end provides the required access to  the CouchDB-Instance.

## :arrow_down:Requirements

If you want to setup Easylist with Docker you can skip this section.

#### Clone the Repository

This repository contains the required source-code. Simply clone it in a folder and you are ready to start.

#### Install CouchDB

1) Install CouchDB (v2.3.1) on your platform. [CouchDB: Installation & First-Time Setup]( https://docs.couchdb.org/en/2.2.0/install/index.html )

2) Create an admin user. [Path to your running CouchDB instance](http://127.0.0.1:5984/_utils/# )

3) Specify the user credentials in the ``application.properties`` file of the AuthService

#### Install Rust

The project uses WebAssembly (compiled from Rust) in the Web-App. In order to build it, you need following tools installed:

1) The Rust language. Setup information can be found [here](https://www.rust-lang.org/tools/install). The project utilizes features from the Rust nightly build, therefore you need to switch your Rust toolchain. You can do it with the following commands:
```
rustup toolchain install nightly
rustup default nightly
```

2) The wasm-pack plugin. It is required to convert Rust source code into WebAssembly. Setup information can be found [here](https://rustwasm.github.io/wasm-pack/installer/).

### Install Gradle

1) Gradle requires Java version 8 or higher. Download Java JRE or JDK from [here](https://www.oracle.com/technetwork/java/javase/downloads/index.html)

2) Install Gradle from [here](https://gradle.org/install/) and make sure it's in your PATH variable

## Usage

### :whale: With Docker (Recommended)

If you just want to test Easylist locally, build the Docker image and run it.

```
sudo docker-compose build
sudo docker-compose up
```

You can tweak the Easylist configuration in the `docker-compose` file, with the following environment variables:

* `HOST`

  Set the ip-address / domain-name which your services will use. For example, if you have a certificate for  `example.com` then set `HOST=example.com`.

* `HTTPS_PORT`

  Set the https port of the Easylist-Webservice.

* `HTTP_PORT`

  Set the http port of the Easylist-Webservice.

* `REDIRECT_TO_HTTPS`

  Redirect all http traffic to https.

  > Note: The redirect will always use Port 443 for https. So if you want to use the redirect option use 443 for the `HTTPS_PORT` in the configuration.

When using https a self-signed certificate will be used as default.

If you want to use your own certificate, replace the `easylist_cert.p12`  certificate in the `_certificates` folder with your own one. 

> Note: The certificate should be named `easylist_cert.p12` and use the `PKCS 12` format.

Now you can build and run the Easylist service:
```
sudo docker-compose build
sudo docker-compose up
```

### :elephant: With Gradle
Adapt the services to your infrastructure via their `application.properties` files, found under `src/main/resoures` in the respective folders of the services.

Then you can start every service with:
```
(cd DiscoveryService && gradle bootRun) & \
(cd AuthService && gradle bootRun & \
(cd WebService && gradle wasmBuild && gradle appInstall && gradle appBuild && gradle appCopy && gradle bootRun)
```



## :hammer_and_wrench:Build

You need to be in the path for the gradle commands to be available

#### DiscoveryService

`gradle build`: Build DiscoveryService

#### AuthService

`gradle build`: Build AuthService

#### WebService

`gradle wasmBuild`: Build WebAssembly 

`gradle appInstall`: Install React dependencies via NPM

`gradle appBuild`: Build React App

`gradle appCopy`: Copy the build output of the React App to Spring resources folder

`gradle build`: Build Spring Project

## :bicyclist:Run

Services need to be started in the following order:

#### DiscoveryService

`gradle bootRun`: Run DiscoveryService

#### AuthService

`gradle bootRun`: Run AuthService

#### WebService

`gradle bootRun`: Run WebService on Port 8090

## :sparkles:Features

Our application provides the following features:

- [x] Offline availability
- [x] You can create, edit and delete articles
- [x] You can mark an article as bought
- [x] A trash is provided, to prevent something from getting deleted accidentally
- [x] You can show and hide articles depending on their group
- [x] Deployable via Docker 
- [x] System available globally (ask project team for showcase)

## :microscope:Test



## :books:Documentation

Our Overleaf-Documentation can be found [here](https://www.overleaf.com/read/zyvyqczsxmty).
