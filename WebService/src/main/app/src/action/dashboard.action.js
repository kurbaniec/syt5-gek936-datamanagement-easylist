
export const SAVE_TAB = "SAVE_TAB";
export const SAVE_CAT = "SAVE_CAT";

export const saveTab = (tabselected) => ({
    type: SAVE_TAB,
    tabselected
});

export const saveCat = (tabselected, catselected) => ({
    type: SAVE_CAT,
    tabselected,
    catselected
});
