import React, { Component } from 'react'
import './globalWorker'

export default class AddItem extends Component{
    constructor(props) {
        super(props)
        this.state = {
            name: "",
            amount: "",
            bought: false,
            group: "Supermarkt",
            deleted: false
        }
        this.saveItem = this.saveItem.bind(this);
    }

    tsave =() => {
        worker.postMessage(['save', {
            'name':this.state.name,
            'amount':this.state.amount,
            'bought':this.state.bought,
            'deleted':this.state.deleted,
            'group':this.state.group
        }]);
    }

    saveItem = (e) => {
        e.preventDefault();
        this.tsave();
        window.location.replace("../crud");
    }

    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value});
    }
    render() {
        return (
            <div>
                <h2 className="text-center">Add item</h2>
                <form>
                    <div className="form-group">
                        <label>Item Name:</label>
                        <input type="text" placeholder="name" name="name" className="form-control" value={this.state.name} onChange={this.onChange}/>
                    </div>

                    <div className="form-group">
                        <label>Amount:</label>
                        <input type="text" placeholder="amount" name="amount" className="form-control" value={this.state.amount} onChange={this.onChange}/>
                    </div>

                    <div className="form-group">
                        <label>Group:</label>
                        <input type="text" placeholder="e.g. supermarket" name="group" className="form-control" value={this.state.group} onChange={this.onChange}/>
                    </div>
                    <div className="form-group">
                        <label>Bought: </label>
                        <input type="checkbox" name="bought" value={this.state.bought} onChange={this.onChange}/>
                    </div>



                    <button className="btn btn-success" onClick={this.saveItem}>Save</button>
                </form>
            </div>
        );
    }
}