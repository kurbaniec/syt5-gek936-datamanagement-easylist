export const authConstants = {
    loggedIn: "USER_LOGIN_STATE",
    verified: "DATABASE_DECRYPED_STATE"
};