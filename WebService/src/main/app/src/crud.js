import React, { Component } from 'react'
import './globalWorker'

export default class Crud extends Component{
    constructor(props) {
        super(props)
        this.state = {
            list: [],
            enabledGroups: [],
            showDeleted: false,

        };
        this.deleteItem = this.deleteItem.bind(this);
        this.editItem = this.editItem.bind(this);
        this.addItem = this.addItem.bind(this);
        this.reloadItemList = this.reloadItemList.bind(this);
        if(worker!=null){
            worker.terminate();
            worker = new Worker('worker.js');
        }
        else{
            worker = new Worker('worker.js');
        }
        worker.addEventListener('message', this.reloadItemList, false);
    }
    componentDidMount() {
        this.initializeGroups();
    }

    /**
     * Sort by: group. In a group sort by deleted. If they are both deleted/not deleted name sort by name
     * @param a
     * @param b
     * @returns {number}
     */
    compare(a,b) {
        if(a.group>b.group){
            return 2;
        }else if (a.group<b.group){
            return -2;
        }
        else if(a.deleted){
            return 1
        }
        else if (b.deleted){
            return -1;
        }
        else if(a.name>b.name){
            return 1;
        }else if(a.name<b.name){
            return -1;
        }

        return 0;
    }

    reloadItemList = (e) =>  {
        if(e!=undefined){
            const cmd = e.data[0];
            const data = e.data[1];
            switch (cmd) {
                case 'all': // Get all values from database
                    this.state.list = [];
                    for (let i = 0; i < data.length; i++) {
                        const doc = data[i].doc;
                        if (doc.name !== undefined) {
                            this.state.list.push(doc);
                        }
                    }
                    break;
                case 'save':
                    this.state.list.push(data);
                    const identification = data._id.split("::")[0];
                    break;
                case 'update':
                    const index = this.getEntry(data._id);
                    this.state.list[index] = data;

            }
            this.forceUpdate();
        }
    }

    tupdate(id) {
        const index = this.getEntry(id);
        worker.postMessage(['update',{
            '_id':this.state.list[index]._id,
            '_rev':this.state.list[index]._rev,
            'name':this.state.list[index].name,
            'amount':this.state.list[index].amount,
            'bought':this.state.list[index].bought,
            'deleted':this.state.list[index].deleted,
            'group':this.state.list[index].group
        }]);
    }

    deleteItem(itemId) {
        const index = this.getEntry(itemId);
        this.state.list[index].deleted = true;
        this.tupdate(itemId);
        this.forceUpdate();
    }
    getEntry = (id) => {
        for (let i = 0; i < this.state.list.length; i++) {
            if (this.state.list[i]._id === id) {
                return i;
            }
        }
    }

    tremove = (id) => {
        const index = this.getEntry(id);
        const theid = this.state.list[index]._id;
        const therev = this.state.list[index]._rev;
        worker.postMessage(['remove', {'selector': {'_id': theid, '_rev': therev}}]);
        this.forceUpdate();
    }

    editItem(id) {
        this.state.list.map(
            item =>
            {
                if(item._id==id){
                    window.localStorage.setItem("_id", item._id);
                    window.localStorage.setItem("_rev", item._rev);
                    window.localStorage.setItem("name", item.name);
                    window.localStorage.setItem("amount", item.amount);
                    window.localStorage.setItem("bought", item.bought);
                    window.localStorage.setItem("group", item.group);
                    window.localStorage.setItem("deleted", item.deleted);
                }
            }
            )

        this.props.history.push('/edit-item');
    }

    addItem() {
        window.localStorage.removeItem("_id");
        //var saved = worker.postMessage({'cmd': 'save', 'db': 'UserDB'}, userdata);
        this.props.history.push('/add-item');
    }
    changeBought = (id) => {

        this.state.list.map(
            item =>
            {
                if(item._id==id){
                    if(item.bought){
                        item.bought=false;
                    }
                    else{
                        item.bought = true
                    }
                }
            }
        )
        this.tupdate(id);
        this.forceUpdate();
    }

    changeShowDeleted = () => {
        if(this.state.showDeleted){
            this.setState({showDeleted: false});
        }
        else{
            this.setState({showDeleted: true});
        }
        this.forceUpdate();
     }

    /**
     * returns an array of all existing groups
     * @returns {Array}
     */
     getExistingGroups () {
        let groups = new Array();
        this.state.list.map(
            item =>
            {
                if(!groups.some(tmp => tmp == item.group)){
                    groups.push(item.group);
                }
            }
        )
         return groups;
     }

     initializeGroups(){
         this.state.enabledGroups =  this.getExistingGroups();
     }
     restore = (id) =>{
         const index = this.getEntry(id);
         this.state.list[index].deleted = false;
         this.tupdate(id);
         this.forceUpdate();
     }

     changeGroupState = (name) => {

         if(this.state.enabledGroups.some(grp => grp == name)){
             let index = this.state.enabledGroups.indexOf(name);
             this.state.enabledGroups.splice(index, 1);
         }
         else{
             this.state.enabledGroups.push(name);
         }
         this.forceUpdate();
     }
    render() {
        this.state.list.sort(this.compare);
        let allGroups =this.getExistingGroups();
        return (
            <div>
                <h2 className="text-center">A list of all items</h2>
                <button className="btn btn-primary" onClick={() => this.addItem()}> Add Item</button>
                <button className="btn btn-info" onClick={this.changeShowDeleted}>{this.state.showDeleted ? "Hide deleted": "Show deleted"}</button>
                {
                allGroups.map(
                    group =>
                        <div key={group} >
                            {group} : <input type="checkbox" onChange={() => this.changeGroupState(group)} checked={this.state.enabledGroups.some(grp => grp == group)}/>
                        </div>
                    )
                }
                <table className="table table-striped">
                    <thead>
                    <tr key="head">
                        <th>Name</th>
                        <th>Amount</th>
                        <th>Group</th>
                        <th>Bought</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.list.map(
                            item =>
                                /*
                                shows an item it's group is within the enabled group and
                                 */
                                <tr key={item._id} className={this.state.enabledGroups.some(grp => grp == item.group) ? item.deleted ? (this.state.showDeleted ? 'deleted' : 'hidden') : '' : "hidden"} >
                                    <td>{item.name}</td>
                                    <td>{item.amount}</td>
                                    <td>{item.group}</td>
                                    <td><input type="checkbox" value={item.bought} onChange={() => this.changeBought(item._id)} checked={item.bought}/></td>
                                    <td>
                                        <button className={item.deleted ? "hidden" : "btn btn-danger"} onClick={() => this.deleteItem(item._id)}> Delete</button>
                                        <button className={item.deleted ? "hidden" : "btn btn-success"} onClick={() => this.editItem(item._id)}> Edit</button>
                                        <button className={!item.deleted ? "hidden" : "btn btn-danger"} onClick={() => this.restore(item._id)}> Restore</button>
                                        <button className={!item.deleted ? "hidden" : "btn btn-danger"} onClick={() => this.tremove(item._id)}>Remove from thrash</button>
                                    </td>
                                </tr>
                        )
                    }
                    </tbody>
                </table>

            </div>
        );
    }
}
