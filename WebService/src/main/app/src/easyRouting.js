import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import EditItem from "./edit-item"
import AddItem from "./add-item"
import Crud from "./crud"
const AppRouter = () => {
    return(
        <div>
            <Router>
                <div className="col-md-6">
                    <h1 className="text-center" style={style}>EasyList</h1>
                    <Switch>
                        <Route path="/" exact component={Crud} />
                        <Route path="/crud" component={Crud} />
                        <Route path="/add-item" component={AddItem} />
                        <Route path="/edit-item" component={EditItem} />
                    </Switch>
                </div>
            </Router>
        </div>
    )
}

const style = {
    margin: '10px'
}

export default AppRouter;