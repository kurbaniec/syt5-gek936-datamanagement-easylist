import React, { Component } from 'react'
import './globalWorker'

export default class EditItem extends Component{
    constructor(props) {
        super(props)
        this.state = {
            _id: "",
            _rev: "",
            name: "",
            amount: "",
            bought: false,
            group: "",
            deleted: true,
        }
        this.saveItem = this.saveItem.bind(this);
        this.loadItem = this.loadItem.bind(this);
    }
    componentDidMount() {
        this.loadItem();
    }

    tupdate = () => {
        worker.postMessage(['update',{
            '_id':this.state._id,
            '_rev':this.state._rev,
            'name':this.state.name,
            'amount':this.state.amount,
            'bought':this.state.bought,
            'deleted':this.state.deleted,
            'group':this.state.group
        }]);
    }

    saveItem = (e) =>{

        e.preventDefault();
        this.tupdate()
        window.location.replace("../crud");
        //var saved = worker.postMessage({'cmd': 'save', 'db': 'UserDB'}, userdata);
    }
    loadItem(){
        let _id = window.localStorage.getItem("_id");
        let _rev = window.localStorage.getItem("_rev");
        let name = window.localStorage.getItem("name");
        let amount =window.localStorage.getItem("amount");
        let bought = (window.localStorage.getItem("bought")== 'true');
        let group = window.localStorage.getItem("group");
        let deleted = (window.localStorage.getItem("deleted")=='true');
        this.setState({_id: _id, _rev: _rev, name: name, amount: amount, bought: bought, deleted: deleted, group: group});
        this.forceUpdate();
    }

    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value});
        this.forceUpdate();
        console.log("ICH HAB DJAOPSÄKDÖ")
    }
    changeBought = () =>{
        if(this.state.bought){
            this.setState({bought: false})
        }
        else{
            this.setState({bought: true})
        }
        console.log(this)
    }

    render() {
        return (
            <div>
                <h2 className="text-center">Edit Item</h2>
                    <div className="form-group">
                        <label>Item Name:</label>
                        <input type="text" placeholder="name" name="name" className="form-control" value={this.state.name} onChange={this.onChange}/>
                    </div>

                    <div className="form-group">
                        <label>Amount:</label>
                        <input type="text" placeholder="amount" name="amount" className="form-control" value={this.state.amount} onChange={this.onChange}/>
                    </div>

                <div className="form-group">
                    <label>Group:</label>
                    <input type="text" placeholder="e.g. supermarket" name="group" className="form-control" value={this.state.group} onChange={this.onChange}/>
                </div>

                    <div className="form-group">
                        <label>Bought: </label>
                        <input type="checkbox" name="bought" onChange={this.changeBought} checked={this.state.bought} />
                    </div>


                    <button className="btn btn-success" onClick={this.saveItem}>Save</button>
            </div>
        );
    }
}
