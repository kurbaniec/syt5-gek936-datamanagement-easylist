import React from "react";
import ReactDOM from "react-dom";
import {
    BrowserRouter as Router,
    Switch,
    Redirect,
    Route
} from "react-router-dom";


import "./index.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import {createStore, applyMiddleware} from "redux";
import rootReducer from "./store/reducers/root.reducer";
import {Provider} from "react-redux"
import thunk from "redux-thunk";
import AppRoute from "./easyRouting";
import * as serviceWorker from "./service-worker/sw-handler";


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();


// Für Storage
const store = createStore(rootReducer, applyMiddleware(thunk));


// Grundapp
class App extends React.Component {

    constructor(state) {
        super(state);
    }

    getApp() {
        return (
            <div className="App">
                <AppRoute/>
            </div>
        );


    }

    render() {
        return this.getApp();
    }
}
// Ins Grundgerüst setzen
const rootElement = document.getElementById("root");
ReactDOM.render(<Provider store={store} key={store}><Router><App /></Router></Provider>, rootElement);
