importScripts("bower_components/pouchdb/dist/pouchdb.min.js");
importScripts("bower_components/pouchdb/dist/pouchdb.find.min.js");
import("../../rust/pkg").then(wasm => {

    const dev = process.env.NODE_ENV === 'development';

    let worker = null;
    let remoteInit = false;

    const setRemote = async () => {
        if (dev) {
            worker = new wasm.Worker("http://localhost:5984/easylist");
        } else {
            const url = await fetch("/database");
            const response = await url.json();
            worker.set_remote(response.db);
        }
        remoteInit = true;
    };

    // Initializes Functionality
    const init = async () => {
        // Create connection to remote database if online
        if (navigator.onLine) {
            if (dev) {
                worker = new wasm.Worker("http://localhost:5984/easylist");
            } else {
                const url = await fetch("/database");
                const response = await url.json();
                const db = response.db.replace("127.0.0.1", "localhost");
                console.log("Database: " + db);
                worker = new wasm.Worker(db);
                console.log("Initialized worker");
                //worker = new wasm.Worker("https://localhost:7000/auth/couchdb/easylist");
            }
            remoteInit = true;
        // When offline, the remote setup will be done when a connection
        // is found
        } else {
            worker = new wasm.Worker("");
        }
        self.postMessage('initDone');
        heartbeat();
    };

    const sleep = async (ms) => {
        return new Promise(resolve => setTimeout(resolve, ms));
    };

    const heartbeat = async () => {
        while (true) {
            if (navigator.onLine) {
                // If worker started in offline mode and now goes online
                // connect to the remote database
                if (!remoteInit) {
                    await setRemote();
                }
                worker.set_service_status("online");
                console.log("online")
            } else {
                worker.set_service_status("offline");
                console.log("offline");
            }
            try {
                console.log(JSON.stringify(await worker.check()));
                const all = await worker.all_docs();
                self.postMessage(['all', all.rows]);
                await sleep(3000);
            } catch (e) {
                console.error("Something went wrong");
                console.error(e);
            }
        }
    };

    init();

    self.addEventListener('message', async function(e) {
        const cmd = e.data[0];
        const data = e.data[1];
        switch (cmd) {
            case 'test':
                let msg = await worker.process(data.msg);
                self.postMessage(data.msg + ': ' + msg);
                break;
            case 'save':
                await worker.save(data);
                const ret = await worker.find({"selector":{"name": data.name }});
                self.postMessage(['save', ret.docs[0]]);
                break;
            case 'update':
                const updateReturn = await worker.update(data);
                if (updateReturn.ok === true) {
                    console.log("Update successfull");
                } else {
                    console.log("Update NOT successfull");
                }
                const newData = await worker.find({"selector":{"_id": data._id}});
                self.postMessage(['update', newData.docs[0]]);
                break;
            case 'remove':
                const rem = await worker.find(data);
                const result = await worker.remove(rem.docs[0]);
                break;
            case 'find':
                let query = await worker.find(data);
                self.postMessage('Found documents: ' + JSON.stringify(query.docs));
                break;
            case 'all':
                const all = await worker.all_docs();
                self.postMessage(['all', all.rows]);
                break;
            case 'stop':
                self.postMessage('WORKER STOPPED: ' + data.msg + '. (buttons will no longer work)');
                self.close(); // Terminates the worker.
                break;
            default:
                self.postMessage('Unknown command: ' + data.msg);
        }
    }, false);
});

