package dev.easypass.web.controller

import com.netflix.appinfo.InstanceInfo
import com.netflix.discovery.EurekaClient
import com.netflix.discovery.shared.Application
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.MediaType
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletResponse


/**
 *
 *
 * @author Kacper Urbaniec
 * @version 2019-10-17
 */

@Controller
class Controller {
    @Qualifier("eurekaClient")
    @Autowired
    private val eurekaClient: EurekaClient? = null

    @RequestMapping(value = ["/"])
    fun index(): String {
        return "index"
    }

    @RequestMapping(value = ["/crud"])
    fun crud(): String {
        return "index"
    }

    @RequestMapping(value = ["/backendtest"])
    fun test(): String {
        return "backendtest"
    }

    @RequestMapping(value = ["/test"])
    @ResponseBody
    fun index2(): String {
        return "<h1>Test!</h1>"
    }

    /**
    // TODO Get data from Eureka service
    @RequestMapping(value = ["/redirect"], method = [RequestMethod.GET], produces = ["application/json"])
    @ResponseBody
    @CrossOrigin(origins = ["*"])
    fun method(): Map<String, String> {
        return mapOf("db" to "http://10.0.105.130:7000/couchdb/easylist")
    }*/

    @RequestMapping(value = ["/database"], method = [RequestMethod.GET], produces = ["application/json"])
    @ResponseBody
    @CrossOrigin(origins = ["*"])
    fun getIPAddress(): Map<String, String> {
        val serviceID = "auth-service"
        val application: Application = eurekaClient!!.getApplication(serviceID)
        val instanceInfo: InstanceInfo = application.instances[0]
        val url = instanceInfo.homePageUrl
        print(url)
        return mapOf("db" to url + "auth/couchdb/easylist")
    }


}