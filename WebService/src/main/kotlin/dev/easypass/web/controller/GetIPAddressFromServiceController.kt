package dev.easypass.web.controller

import com.netflix.appinfo.InstanceInfo
import com.netflix.discovery.EurekaClient
import com.netflix.discovery.shared.Application
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController


@RestController
class GetIPAddressFromServiceController {
    @Autowired
    private val eurekaClient: EurekaClient? = null
    
    @GetMapping("/ipaddress/{serviceID}")
    fun getIPAddress(@PathVariable serviceID: String = "auth-service"): String {
        val application: Application = eurekaClient!!.getApplication(serviceID)
        val instanceInfo: InstanceInfo = application.instances[0]
        val ip = instanceInfo.ipAddr
        println(ip)
        return ip
    }
}
