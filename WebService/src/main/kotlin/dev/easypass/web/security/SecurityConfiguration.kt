package dev.easypass.web.security

import com.netflix.discovery.DiscoveryClient
import org.apache.http.ssl.SSLContextBuilder
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import java.io.File
import javax.net.ssl.SSLContext


/**
 *
 *
 * @author Kacper Urbaniec
 * @version 2019-10-17
 */

@Configuration
class SecurityConfiguration : WebSecurityConfigurerAdapter() {

    @Value("\${redirect}")
    private val redirect = false

    @Throws(Exception::class)
    override fun configure(httpSecurity: HttpSecurity) {
        if (redirect) {
            httpSecurity.requiresChannel().anyRequest().requiresSecure()
        }
        httpSecurity.authorizeRequests().antMatchers("/").permitAll()
    }

}


@Configuration
class SslConfiguration {
    @Value("\${server.ssl.key-store}")
    private val trustStore: File? = null
    @Value("\${server.ssl.key-store-password}")
    private val trustStorePassword: String? = null

    @Bean
    fun getTrustStoredEurekaClient(sslContext: SSLContext?): DiscoveryClient.DiscoveryClientOptionalArgs {
        val args = DiscoveryClient.DiscoveryClientOptionalArgs()
        args.setSSLContext(sslContext)
        return args
    }

    @Bean
    @Throws(Exception::class)
    fun sslContext(): SSLContext {
        print("initialize ssl context bean with keystore " + trustStore)
        return SSLContextBuilder()
                .loadTrustMaterial(
                        trustStore,
                        trustStorePassword!!.toCharArray()
                ).build()
    }
}



