# Certificates in Easylist

## Generate self-signed certificates

Generate simple certificate for `localhost`:

```
keytool -genkey -alias easylist_cert -keyalg RSA -keysize 2048 -validity 700 -keypass changeit -storepass changeit -keystore easylist_cert.jks
```

Convert JKS Keystore to a PKCS12 format:

```
keytool -importkeystore -srckeystore easylist_cert.jks -destkeystore easylist_cert.p12 -srcstoretype JKS -deststoretype PKCS12 -deststorepass changeit
```



## Sources

* [Create your own self signed SSL certificate | 14.01.2020](https://howtodoinjava.com/spring-boot/spring-boot-ssl-https-example/)
* [How to convert a JKS Keystore to a PKCS12 (.p12) format | 14.01.2020](https://knowledge.digicert.com/solution/SO17389.html)

